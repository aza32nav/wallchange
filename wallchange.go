package main

import (
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"time"
)

const ADJUST = "'scaled'"
const PATH = "/Wallpapers/"
const MINUTES = 17

func main() {
	path := os.Getenv("HOME") + PATH
	fullPath := ""
	randnum := 0

	// Random number
	source := rand.NewSource(time.Now().UnixNano())
	rand1 := rand.New(source)

	// Read files in a Dir
	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for {
		// make the path with a random file
		randnum = rand1.Intn(len(files))
		fullPath = path + files[randnum].Name()

		// run commands with a path
		commandsRun(fullPath)
		//time sleep
		time.Sleep(MINUTES * time.Minute)
	}
}

func commandsRun(path string) {
	cmdFit := exec.Command("gsettings", "set", "org.gnome.desktop.background",
		"picture-options", ADJUST)
	cmdWall := exec.Command("gsettings", "set", "org.gnome.desktop.background",
		"picture-uri", path)

	err := cmdFit.Run()
	if err != nil {
		log.Fatal("Error when executing command picture-options:", err)
	}

	err = cmdWall.Run()
	if err != nil {
		log.Fatal("Error when executing command picture-uri:", err)
	}
}
